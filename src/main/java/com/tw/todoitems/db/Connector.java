package com.tw.todoitems.db;

import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Connector {
    private Config config;

    public Connector() {
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream("db-config.yml");
        this.config = yaml.load(inputStream);
    }

    public Config getConfig() {
        return config;
    }

    public Connection createConnect() throws SQLException {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(config.getDatabaseURL(),
                    config.getUsername(), config.getPassword());
        } catch (SQLException e) {
            System.out.println("Connection failed");
        }
        return conn;
    }

//    public static void main(String[] args) {
//        Connector conn = new Connector();
//        System.out.println(conn.getConfig());
//    }
}

