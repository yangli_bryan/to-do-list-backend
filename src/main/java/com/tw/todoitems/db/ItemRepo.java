package com.tw.todoitems.db;

import com.tw.todoitems.model.Item;
import com.tw.todoitems.model.ItemStatus;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class ItemRepo {
    private Connector connector;

    public ItemRepo() {
        this.connector = new Connector();
    }

    public Item addItem(Item item) {
        String query = "INSERT INTO todoitems (text, status) VALUES (\"" + item.getText()
                + "\", \"" + item.getStatus() + "\");";
        String extractId = "SELECT id FROM todoitems WHERE text = \"" + item.getText() +"\" LIMIT 1;";
        try {
            Connection conn = connector.createConnect();
            Statement statement = conn.createStatement();
            statement.executeUpdate(query);

            ResultSet rs = statement.executeQuery(extractId);
            rs.next();
            item.setId(rs.getInt("id"));

            conn.close();
            return item;
        } catch (SQLException error) {
            System.out.println("Failed adding item.");
        }
        return null;
    }

    public List<Item> findItems() {
        String extractItems = "SELECT * FROM todoitems";
        List<Item> resultList = new ArrayList<Item>();
        try {
            Connection conn = connector.createConnect();
            Statement statement = conn.createStatement();
            ResultSet rs = statement.executeQuery(extractItems);
            while (rs.next()) {
                Item newItem = new Item(rs.getString("text"));
                newItem.setId((rs.getInt("id")));
                ItemStatus itemStatus = rs.getString("status").equals("ACTIVE") ? ItemStatus.ACTIVE : ItemStatus.COMPLETED;;
                newItem.setStatus(itemStatus);
                resultList.add(newItem);
            }

            conn.close();
        } catch (SQLException error) {
            System.out.println("Failed looking up items");
        }
        return resultList;
    }

    public boolean updateItem(Item item) {
        String updateItem = "Update todoitems SET text = \"" + item.getText() +
                "\", status = \"" + item.getStatus() +"\" WHERE id = " + item.getId() + ";";
        boolean result = false;
        try {
            Connection conn = connector.createConnect();
            Statement statement = conn.createStatement();
            statement.executeUpdate(updateItem);
            result = true;
            conn.close();
        } catch (SQLException error) {
            System.out.println("Failed updating item info.");
        }
        return result;
    }

    public boolean deleteItem(int id) {
        String deleteItem = "DELETE FROM todoitems WHERE id = " + id;
        String queryCount = "SELECT count(*) AS numberOfItems FROM todoitems";
        String resetIndex = "ALTER TABLE todoitems AUTO_INCREMENT = 1";
        boolean result = false;
        try {
            Connection conn = connector.createConnect();
            Statement statement = conn.createStatement();
            statement.executeUpdate(deleteItem);
            ResultSet rs = statement.executeQuery(queryCount);
            rs.next();
            if(rs.getInt("numberOfItems") == 0) {
                statement.execute(resetIndex);
            }
            result = true;
            conn.close();
        } catch (SQLException error) {
            System.out.println("Failed deleting items.");
        }
        return result;
    }

}
